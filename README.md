# Hw_1

Apple Store

In this assignment you will implement an AppleStore class. Please also read the submission
instructions at the end of this document very carefully. There will be no exception to these
rules.

 An AppleStore sells only MacBook therefore the AppleStore class has the following
fields: storeName, exchangeRate, macCost, macPrice and soldMacCount. (Use this
ordering of fields in your function calls if you need to use multiple function arguments).

 storeName is the name of the store. An example can be “Zorlu Apple Store”.

 This AppleStore class will be used in Apple Stores in Turkey. An AppleStore has its prices
in US Dollars. For example, macCost is the manufacturing cost of a MacBook including all
necessary taxes. macPrice is the customer price of a MacBook. Both macCost and
macPrice is in US dollars. Difference between these two is the earned profit of a
MacBook.

 Apple Stores in Turkey only accepts credit card (no cash) and all their earnings are kept
in Turkish banks in Turkish Liras. Therefore, the amount to be earned will be converted
to Turkish Liras by using the current exchange rate which is kept in exchangeRate field
as soon as the customer’s credit card is swiped in order to complete the sale.

 soldMacCount is the number of macs sold.

 Implement a constructor for this class. Initially the soldMacCount is 0 and the exchange
rate is also set to 7.5 Turkish Liras for 1 US Dollars. Therefore, no need to get an initial
value for these fields from the user. As customers visit the store and buy macs
soldMacCount will increase. The constructor does not output anything to the console.

 Implement a sellMacs function which takes the number of macs sold to a customer as a
function argument. This function outputs a line “NumberOfMacsSold macs
sold”. An example is shown below.

 Implement a setExchangeRate function to update the exchange rate from US Dollars to
Turkish Liras. This function takes the TL equivalent of a US Dollar. This function also
outputs 2 lines in the following format.
The exchange rate has been changed!
The exchange rate: 1 USD = NewRate TL

 You will only use these above methods to modify the exchangeRate and soldMacCount.
Think about what other type of set functions you may need and implement them.

 Implement a getRevenue function which returns the total revenue of the store. Revenue
is the gross income. Revenue of just one MacBook is its price. This function does not
print any output.

 Implement a getProfit function which returns the total profit of the store. Profit is the
remaining amount after subtracting the costs from the income. This function does not
print any output.

Implement a toString method for printing all the information about an AppleStore
object. An example output of a toString method is as shown below.
Apple Store Name: Zorlu Apple Store
The cost of a MacBook: 1200.0 USD
The price of a MacBook: 1500.0 USD
The exchange rate: 1 USD = 7.5 TL

 Implement a printStoreFinancials method for printing all the financial information of an
AppleStore object. This function does not return anything. An example output of the
printStoreFinancials method is as shown below.
Total number of macs sold: 10
Total revenue: 112500.0 TL
Total profit: 22500.0 TL

 Feel free to add any useful class instance (member variable) to your class and/or
implement any needed functions.

 An example output of the program is provided below. In this program

o An AppleStore object is created.
o The object is printed to the console.
o sellMacs function is called twice with parameters 10 and 5.
o printStoreFinancials function is called.
o setExchangeRate function is called with parameter 5.

Apple Store Name: Zorlu Apple Store
The cost of a Macbook: 1200.0 USD
The price of a MacBook: 1500.0 USD
The exchange rate: 1 USD = 7.5 TL
10 macs sold
5 macs sold
Total number of macs sold: 15
Total revenue: 168750.0 TL
Total profit: 33750.0 TL
The exchange rate has been changed!
The exchange rate: 1 USD = 8.0 TL
